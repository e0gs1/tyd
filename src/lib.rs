mod types;
mod utils;

pub use utils::*;

use std::fmt::Debug;
use std::rc::Rc;
use std::collections::VecDeque;



pub type HandleLine<T> = dyn Fn(Option<&Line<T>>, &str) -> Line<T>;
pub type HandleNumberedLine<T> = dyn Fn(Option<&NumberedLine<T>>, &str) -> Line<T>;

#[derive(Debug, Clone, Copy)]
pub enum State {
    Begin,
    Continue,
    End,
    Unknown,
}

#[derive(Debug, Clone)]
pub enum Line<T: Clone> {
    Line(T, State),
    DepthLine(T, State, usize),
    NestedLine(VecDeque<Line<T>>, T, State),
    Unknown,
    Skip,
    SkipCount,
}

#[derive(Debug, Clone)]
pub enum NumberedLine<T: Clone> {
    Line(T, State),
    LineDepth(T, State, usize),
    NestedLine(VecDeque<Line<T>>, T, State),
    Unknown,
    Skip,
    SkipCount,
}

pub fn parser<T: Clone>(f: &'static HandleLine<T>) -> Parser<T> {
    Parser::new(Rc::new(f))
}

pub struct Parser<T: Clone> {
    handle_line: Rc<HandleLine<T>>,
}

impl<T: Clone> Parser<T> {
    pub fn new(f: Rc<HandleLine<T>>) -> Self {
        Self { handle_line: f }
    }

    pub fn parse<'a>(&mut self, data: &'a str) -> std::str::Lines<'a> {
        data.lines()
    }

    pub fn iter<'a>(&mut self, data: &'a str) -> TydIterator<'a, T> {
        TydIterator::new(self.handle_line.clone(), data.lines())
    }
}

pub struct TydIterator<'a, T: Clone> {
    parse_fn: Rc<HandleLine<T>>,
    last_line: Option<Line<T>>,
    lines: std::str::Lines<'a>,
}

impl<'a, T: Clone> TydIterator<'a, T> {
    fn new(parse_fn: Rc<HandleLine<T>>, lines: std::str::Lines<'a>) -> Self {
        Self {
            parse_fn,
            last_line: None,
            lines,
        }
    }
}

impl<'a, T: Clone + Debug> Iterator for TydIterator<'a, T> {
    type Item = (Line<T>, &'a str);

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(line) = self.lines.next() {
            let line_state = (self.parse_fn)(self.last_line.as_ref(), line);

            if let Line::SkipCount = line_state {
            } else {
                self.last_line = Some(line_state.clone());
            }

            return Some((line_state, line));
        }

        None
    }
}
